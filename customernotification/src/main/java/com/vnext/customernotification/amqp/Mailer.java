package com.vnext.customernotification.amqp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 * Posted from Feb 21, 2019, 10:09 10
 *
 * @Author viquynh (vi.quynh.31598@gmail.com)
 */

@Component
class Mailer {

    @Autowired
    private JavaMailSender javaMailService;

    public void sendMail(String email){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject("Registration");
        mailMessage.setText("Successfully Registered");
        javaMailService.send(mailMessage);
    }
}



