package com.vnext.customernotification.amqp;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.amqp.core.Queue;
/**
 * Posted from Feb 21, 2019, 10:08 10
 *
 * @Author viquynh (vi.quynh.31598@gmail.com)
 */
@Component
public class Receiver {
    @Autowired
    Mailer mailer;

//    @Bean
//    Queue queue() {
//        return new Queue("CustomerQ", false);
//    }

    @RabbitListener(queues = "CustomerQ")
    public void processMessage(String email) {
        System.out.println(email);
        mailer.sendMail(email);
    }
}
