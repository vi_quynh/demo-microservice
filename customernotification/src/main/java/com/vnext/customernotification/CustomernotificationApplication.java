package com.vnext.customernotification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.annotation.Bean;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.mail.javamail.JavaMailSenderImpl;

@SpringBootApplication
public class CustomernotificationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomernotificationApplication.class, args);
	}

//	@Bean
//	public JavaMailSender javasMailSender(){
//		return new JavaMailSenderImpl();
//	}

}
