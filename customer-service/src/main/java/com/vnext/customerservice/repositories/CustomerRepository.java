package com.vnext.customerservice.repositories;

import com.vnext.customerservice.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

/**
 * Posted from Feb 20, 2019, 15:44 15
 *
 * @Author viquynh (vi.quynh.31598@gmail.com)
 */

@RepositoryRestResource
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findByName(@Param("name") String name);
}
