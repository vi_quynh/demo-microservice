package com.vnext.customerservice.amqp;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;

/**
 * Posted from Feb 20, 2019, 17:28 17
 *K
 * @Author viquynh (vi.quynh.31598@gmail.com)
 */
@Component
@Lazy
public class Sender {

    @Autowired
    RabbitMessagingTemplate template;

//    @Bean
//    Queue queue() {
//        return new Queue("CustomerQ", false);
//    }
    public void send(String message){
        template.convertAndSend("CustomerQ", message);
    }

}
