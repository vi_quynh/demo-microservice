package com.vnext.customerservice.controller;

import com.vnext.customerservice.amqp.Sender;
import com.vnext.customerservice.model.Customer;
import com.vnext.customerservice.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Posted from Feb 20, 2019, 17:16 17
 *
 * @Author viquynh (vi.quynh.31598@gmail.com)
 */
@Component
@Lazy
public class CustomerRegistrar {

    CustomerRepository customerRepository;
    Sender sender;

    @Autowired
    CustomerRegistrar(CustomerRepository customerRepository, Sender sender) {
        this.customerRepository = customerRepository;
        this.sender = sender;
    }

    Customer register(Customer customer) {
        Optional<Customer> existingCustomer = customerRepository.findByName(customer.getName());
        if (existingCustomer.isPresent()) {
            throw  new RuntimeException("is already exist");
        } else {
            customerRepository.save(customer);
            sender.send(customer.getEmail());
        }
        return customer;
    }
}
