package com.vnext.customerservice.controller;

import com.vnext.customerservice.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Posted from Feb 20, 2019, 16:55 16
 *
 * @Author viquynh (vi.quynh.31598@gmail.com)
 */
@RestController
public class CustomerController {
    @Autowired
    CustomerRegistrar customerRegistrar;

    @PostMapping(path = "/register")
    Customer register(@RequestBody Customer customer) {
        return customerRegistrar.register(customer);
    }
}
