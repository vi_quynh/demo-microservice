package com.example.cartservices.cartservice;

import com.example.cartservices.cartservice.model.Greet;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

/**
 * Posted from Feb 20, 2019, 10:52 10
 *
 * @Author viquynh (vi.quynh.31598@gmail.com)
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class GreetingControllerTests {
    @Test
    public void testVanillaService() {
        RestTemplate restTemplate = new RestTemplate();
        Greet greet = restTemplate.getForObject("http://localhost:8080", Greet.class);
        Assert.assertEquals("Hello Fucking World!", greet.getMessage());
    }
}
