package com.example.cartservices.cartservice.controller;

import com.example.cartservices.cartservice.model.Greet;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Posted from Feb 20, 2019, 10:47 10
 *
 * @Author viquynh (vi.quynh.31598@gmail.com)
 */
@RestController
public class GreetingController {

    @RequestMapping("/greeting")
    @ResponseBody
    public HttpEntity<Greet> greeting(@RequestParam(value = "name", required = false, defaultValue = "HATEOAS") String name) {
//        Greet greet = new Greet("Hello" + name);
//        greet.add(linkTo(methodOn(GreetingController.class).greeting(name)).withSelfRel());
//        return new ResponseEntity<>(greet, HttpStatus.OK);
        return null;
    }
}
