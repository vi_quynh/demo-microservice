package com.example.cartservices.cartservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.ResourceSupport;

/**
 * Posted from Feb 20, 2019, 10:49 10
 *
 * @Author viquynh (vi.quynh.31598@gmail.com)
 */
public class Greet extends ResourceSupport {
    @Getter
    @Setter
    private String message;

    public Greet(String message) {
        this.message = message;
    }
}
